" Vim syntax file
" Language:     Syntax highlighting for my personal note syntax
" Maintainer:   Benjamin Schnitzler
" Last Change:  Nov 7, 2013
" Version:      0.1

if exists("b:current_syntax")
  finish
endif

setlocal iskeyword+=:
syn case ignore 

syn match Header      "^HH.*"   contains=HH_Key
syn match Header_I    "^H1.*"   contains=H1_Key
syn match Header_II   "^H2.*"   contains=H2_Key
syn match Header_III  "^H3.*"   contains=H3_Key
syn match HH_Key      "^HH"     contained
syn match H1_Key      "^H1"     contained
syn match H2_Key      "^H2"     contained
syn match H3_Key      "^H3"     contained
syn match Bold        "\.:b .\{-} b:\." contains=BoldStart,BoldEnd
syn match BoldStart   "\.:b "     contained conceal
syn match BoldEnd     " b:\."     contained conceal
syn match Italic      "\.:i .\{-} i:\." contains=ItalicStart,ItalicEnd
syn match ItalicStart   "\.:i "     contained conceal
syn match ItalicEnd     "i:\."     contained conceal
syn match Image      ".:\[img\(|[^|]\{-}\)*]:." contains=ImageStart,ImageEnd
syn match ImageStart   ".:\[img\(|[^|]\{-}\)*|"     contained conceal
syn match ImageEnd     "]:."     contained conceal

"conceal      " conceal requires conceallevel>=2

hi Header      ctermbg=123  ctermfg=black  cterm=bold,underline
"hi HH_Key      conceal

hi Header_I    ctermbg=255   ctermfg=black  cterm=bold
hi H1_Key      ctermbg=177  ctermfg=53  cterm=bold

hi Header_II   ctermbg=141   ctermfg=black  cterm=bold
hi H2_Key      ctermbg=177  ctermfg=53  cterm=bold

hi Header_III  ctermbg=57   ctermfg=white  cterm=bold
hi H3_Key      ctermbg=177   ctermfg=53  cterm=bold

hi Bold cterm=bold
hi Italic cterm=italic
hi Image ctermbg=57

set conceallevel=2

let b:current_syntax = "note"
